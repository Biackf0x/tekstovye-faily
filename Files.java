package ru.peo;

import java.io.*;

/**
 * Реализовать чтение выражений для вычисления из текстового файла и сохранение результатов в текстовом файле.
 * @author Е.О.Пылайкина
 */


public class Files {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));
        String string;
        int result = 0;
        try {
            while ((string = bufferedReader.readLine()) != null) {
                String[] strings = string.split(" ");
                //Split разобьет эту строку по пробелам
               // Например: 2 + 3
                //После split получаем массив 2;+;3
                //Тип данных строка
                int numberOne = Integer.valueOf(strings[0]);
                int numberToo = Integer.valueOf(strings[2]);
                 //switch сравнивает знаки с каждым последующим Совпадением. Если обнаруживается совпадение, то исполняется команда
                    System.out.println(strings[1]);
                switch (strings[1]) {
                    case "+":
                        result = numberOne + numberToo;
                        break;//конец секции.передаёт управление к концу команды switch
                    case "-":
                        //Строковая константа — это нуль или более символов Unicode, заключенных в одинарные или двойные кавычки.
                        // Обычно строковые константы заключают в двойные кавычки, а одинарные кавычки используются только для
                        // тех строк, которые сами содержат двойную кавычку.
                        // Обычно строковые константы заключают в двойные кавычки, а одинарные кавычки используются только для тех строк, которые сами содержат двойную кавычку.
                        result = numberOne - numberToo;
                        break;
                    case "*":
                        result = numberOne * numberToo;
                        break;
                    case ":":
                        result = numberOne / numberToo;
                    default:
                        break;
                }
                //Когда оператор break выполняется в цикле, то досрочно прерывается исполнение оператора цикла, и управление передаётся следующему оператору после цикла.
                bufferedWriter.write(result + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            bufferedReader.close();
            bufferedWriter.close();
        }
    }
}